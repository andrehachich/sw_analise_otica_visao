## Copyright (C) 2019 andre.hachich
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} hardware_model (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: andre.hachich <andre.hachich@JASP38N>
## Created: 2019-11-16

##   166kOhm   208kOhm   255kOhm   297kOhm   374kOhm   470kOhm   806kOhm
##   0.12000   0.11000   0.11000   0.10000   0.10000   0.10000   0.09000
##   0.10000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
##   0.10000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
##   0.10000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
##   0.13000   0.12000   0.11000   0.11000   0.10000   0.10000   0.09000
##   0.11000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
##   0.11000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
##   0.11000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
##   0.11000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
##   0.13000   0.13000   0.12000   0.12000   0.11000   0.11000   0.10000
##   0.11000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
##   0.12000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
##   0.12000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
##   0.12000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
##   0.15000   0.14000   0.14000   0.13000   0.13000   0.12000   0.11000
##   0.13000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
##   0.13000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
##   0.14000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
##   0.14000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
##   0.17000   0.16000   0.16000   0.15000   0.15000   0.14000   0.13000
##   0.15000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
##   0.16000   0.00000   0.00000   0.00000   0.00000   0.00000   0.00000
##   0.20000   0.17000   0.00000   0.00000   0.00000   0.00000   0.00000
##   0.21000   0.20000   0.18000   0.00000   0.00000   0.00000   0.00000
##   0.24000   0.22000   0.21000   0.21000   0.19000   0.19000   0.18000
##   0.29000   0.25000   0.24000   0.23000   0.22000   0.21000   0.20000
##   0.57000   0.34000   0.29000   0.28000   0.25000   0.25000   0.22000
##   1.28000   1.02000   0.74000   0.56000   0.37000   0.32000   0.27000
##   2.00000   1.83000   1.66000   1.54000   1.35000   1.16000   0.76000
##   2.32000   2.21000   2.09000   2.00000   1.87000   1.74000   1.45000
##   2.62000   2.55000   2.49000   2.43000   2.35000   2.27000   2.10000
##   2.98000   2.97000   2.95000   2.94000   2.94000   2.91000   2.89000

## White current (uA)
##221
##213
##205
##197
##189.7
##182
##174.3
##166.9
##159.4
##152
##144.3
##136.6
##129.1
##121.2
##113.5
##105.7
##98
##90.5
##82.8
##75.1
##67.4
##59.8
##52.4
##44.8
##37.3
##29.9
##22.7
##15.9
##9.3
##6
##3.6
##0.4

function [voltage_seed, voltage_hole, time_array] = hardware_model (wave_struct)

  seed_linear_coefficient = 0.1969 * wave_struct.seed_resistor + 88853;
  hole_linear_coefficient = 0.1969 * wave_struct.hole_resistor + 88853;
  
  saturation_coefficient = 563;
  saturation_constant = 0.2288;
  
  
  % Finding intersection between both lines
  seed_intersect = (wave_struct.supply_voltage - saturation_constant) / (seed_linear_coefficient - saturation_coefficient);
  hole_intersect = (wave_struct.supply_voltage - saturation_constant) / (hole_linear_coefficient - saturation_coefficient);
  
  
  time_array = wave_struct.time_array;
  avg_size = 50;
  for i = 1:length(time_array)
    % Convert from current to voltage based on double straight line model
    if (wave_struct.seed_amps(i) < seed_intersect)
      voltage_seed_raw(i) = wave_struct.supply_voltage - seed_linear_coefficient * wave_struct.seed_amps(i);
    else
      voltage_seed_raw(i) = saturation_constant - saturation_coefficient * wave_struct.seed_amps(i);
    endif
    
    % Convert from current to voltage based on double straight line model
    if (wave_struct.hole_amps(i) < hole_intersect)
      voltage_hole_raw(i) = wave_struct.supply_voltage - hole_linear_coefficient * wave_struct.hole_amps(i);
    else
      voltage_hole_raw(i) = saturation_constant - saturation_coefficient * wave_struct.hole_amps(i);
    endif    
    
    %Apply moving average
    voltage_seed(i) = mean(voltage_seed_raw(max(i-avg_size, 1):i));
    voltage_hole(i) = mean(voltage_hole_raw(max(i-avg_size, 1):i));

  endfor

##  subplot(1,2,1)
##  plot(voltage_seed)
##  hold on
##  plot(voltage_hole, "r")
##  
##  clear voltage_seed_raw voltage_hole_raw voltage_seed voltage_hole
##  
##  voltage_seed_raw = wave_struct.supply_voltage - seed_linear_coefficient*wave_struct.seed_amps;
##  voltage_hole_raw = wave_struct.supply_voltage - hole_linear_coefficient*wave_struct.hole_amps;
##  
##  %Apply moving avg
##  for i = 1:length(time_array)
##    voltage_seed_raw(i) = max(voltage_seed_raw(i), 0) + 0.2;
##    voltage_seed_raw(i) = min(voltage_seed_raw(i), wave_struct.supply_voltage);
##    voltage_seed(i) = mean(voltage_seed_raw(max(i-avg_size, 1):i));
##    
##    voltage_hole_raw(i) = max(voltage_hole_raw(i), 0) + 0.2;
##    voltage_hole_raw(i) = min(voltage_hole_raw(i), wave_struct.supply_voltage);
##    voltage_hole(i) = mean(voltage_hole_raw(max(i-avg_size, 1):i));
##  endfor
##  
##  subplot(1,2,2)
##  plot(voltage_seed)
##  hold on
##  plot(voltage_hole, "r")
  
##  waitfor(gcf);
  
endfunction
