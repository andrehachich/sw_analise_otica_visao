## Copyright (C) 2019 andre.hachich
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} add_wave_to_database (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: andre.hachich <andre.hachich@JASP38N>
## Created: 2019-11-07

function [] = add_wave_to_database (filename, vtit, sens, disc, ring, seed, neat, lens, date)
  new_directoryname = ["./database/", vtit, "/", sens,"/"];
  new_filename = [disc,"_",ring,"_",seed,"_",neat,"_",lens," ",date];

  movefile([filename,".csv"],[new_directoryname,"csv/",new_filename,".csv"]);
  movefile([filename,".jpeg"],[new_directoryname,"photo/",new_filename,".jpeg"]);
  movefile(["infowave_",filename,".csv"],[new_directoryname,"info_wave/",new_filename,".csv"]);
endfunction
