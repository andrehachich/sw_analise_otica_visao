## Copyright (C) 2019 andre.hachich
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {} {@var{retval} =} firmware_parameters_estimation (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: andre.hachich <andre.hachich@JASP38N>
## Created: 2019-12-02

function fmw_parameter = firmware_parameters_estimation (wave_struct)

if (strfind(wave_struct.disc, "milho"))
  disc_class = 1;
elseif (strfind(wave_struct.disc, "soja64"))
  disc_class = 7;
elseif (strfind(wave_struct.disc, "soja"))
  disc_class = 2;
elseif (strfind(wave_struct.disc, "algodao"))
  disc_class = 3;
elseif (strfind(wave_struct.disc, "feijao"))
  disc_class = 4;
elseif (strfind(wave_struct.disc, "sorgo"))
  disc_class = 6;
else
  disc_class = 5;
endif

disc_config = struct(...
  "default_def", [019 000 098 038 038 030 000],...  
  "n_furos",     [028 045 054 028 000 045 032],...
  "default_jan", [085 085 085 085 085 085 085],...
  "min_patamar", [060 020 060 060 060 010 020],...
  "thr_min",     [400 400 400 400 400 300 400],...
  "dif_min",     [255 100 100 255 255 100 100],...
  "larg_furo",   [060 060 060 060 060 045 060],...
  "porc_queda",  [060 060 065 060 060 050 060],...
  "max_quad",    [075 075 030 075 075 000 075],...
  "dif_max",     [150 150 200 150 150 100 150]...
  );
##  [
##  %default_def | n_furos | default_jan | min_patamar | thr_min | dif_min | larg_furo | porc_queda | max_quad | dif_max | cult_falha;
##  {19,           28,        85,           60,           400,       255,     60,         60,          75,        150}, %SC_ID_MILHO
##  {0 ,           45,        85,           20,           400,       100,     60,         60,          75,        150}, %SC_ID_SOJA
##  {98,           54,        85,           60,           400,       100,     60,         65,          30,        200}, %SC_ID_ALGODAO
##  {38,           28,        85,           60,           400,       255,     60,         60,          75,        150}, %SC_ID_FEIJAO
##  {38,            0,        85,           60,           400,       255,     60,         60,          75,        150}, %SC_ID_GENERICO
##  {30,           45,        85,           10,           300,       100,     45,         50,          0 ,        100}, %SC_ID_SORGO
##  {0 ,           32,        85,           20,           400,       100,     60,         60,          75,        150}, % SC_ID_SOJA64
##  ];

[hole_max, hole_max_idx]= findpeaks(wave_struct.hole_voltage, "MinPeakDistance",...
                                    round(length(wave_struct.hole_voltage)/ (13/60 * disc_config.n_furos(disc_class) * wave_struct.time_array(end)+5)));
hole_dutycycle = sum(wave_struct.hole_voltage(hole_max_idx(4):hole_max_idx(5)) >= 0.5*mean(hole_max));


fmw_parameter = struct(...
  "max_quad",       disc_config.max_quad(disc_class),... ##PORCENTAGEM A SER APLICADO NO MAXIMO DO SINAL SEM NA JANELA
  "thr_min",        disc_config.thr_min(disc_class),... ##VALOR MINIMO ABSOLUTO A SER COMPARADO AO MAX DO SINAL SEM NA JANELA
  "dif_max_falha",  disc_config.dif_max(disc_class),... ##DELTA A SOMAR DO MINIMO DO PONTO DE TRIGGER
  "porc_queda",     disc_config.porc_queda(disc_class),... ##PORCENTAGEM A SER APLICADO NO PERIODO DA JANELA P/ VERIFICACAO DE POSICAO DE QUEDA DO SINAL DA SEM NA JANELA
  "dif_min",        disc_config.dif_min(disc_class),... ##DELTA A SUBTRAIR DO MINIMO DO PONTO DE TRIGGER
  "val_larg_furo",  disc_config.larg_furo(disc_class)*hole_dutycycle/100,...  ##60%(DEFAULT) do VALOR DO HIGH (c/THR = 50%) DO FURO
  "default_jan",    disc_config.default_jan(disc_class)... ## porcentagem da janela �til
  );



endfunction
