## Copyright (C) 2019 andre.hachich
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} read_csv_file (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: andre.hachich <andre.hachich@JASP38N>
## Created: 2019-11-05

function [csv_data] = read_csv_wave_file (filename)
  
 
  raw_string = ostrsplit(fileread(filename),"\n", true);
  header_end = 1;
  %Extract header from the rest of the file
  for i = 1:length(raw_string)
    % Find list of strings in the line
    line_string = ostrsplit(raw_string{i}, ",;", true); 
    if (isempty(str2num(line_string{1})))
      header_end = i;
    endif
  endfor


  
  %Find info in header
  header_info = struct("Model", "", "Start Time", "0", "Sample Interval", "1");
  
  for i = 1:header_end
    line_string = ostrsplit(raw_string{i}, ";,", true);
    for j = 1:length(fieldnames(header_info))
      line_idx = find(index(line_string, fieldnames(header_info){j}));
      if (!isempty(line_idx))
        %Extract model from header information
        header_info.(fieldnames(header_info){j}) = strtrim(line_string{line_idx+1});
      endif
    endfor
  endfor
  
  %Sample interval needs further processing, checked and converted to numer
  header_info.("Sample Interval") = ostrsplit(header_info.("Sample Interval"), ": ", true);
  for i = 1:length(header_info.("Sample Interval"))    
    if (!isempty(str2num(header_info.("Sample Interval"){i})))
      header_info.("Sample Interval") = str2double(header_info.("Sample Interval"){i});
      break;
    endif
  endfor
  

  
  switch header_info.("Model")
    case "M-SCOPE 60"
      separation_char = ",";
      decimal_char = ".";
    case "34470A"
      separation_char = ";";
      decimal_char = ",";
    case "TBS1102B"
      separation_char = ",";
      decimal_char = ".";

    otherwise
      separation_char = ",";
      decimal_char = ".";
  endswitch
  
%  disp(header_info)  
%  disp(["Separation char  ", separation_char]);
%  disp(["Decimal char     ", decimal_char]);
  
  
  %Delete header information from raw_string
  raw_string(1:header_end) = [];

  
  %If decimal char is comma, substitue by dot
  if (strcmp(",", decimal_char))
    raw_string = strrep(raw_string, decimal_char, ".");
  endif
  
  %Fill time and current arrays with the data from the file
  csv_size = length(ostrsplit(raw_string{1}, separation_char, true));
  csv_data = zeros(length(raw_string), csv_size);
  
  for i = 1:length(raw_string)
    for j = 1:csv_size
      csv_data(i,j) = str2double(ostrsplit(raw_string{i}, separation_char, true)(j));
    endfor
    %If there is a Sample Interval defined, substitute time by multiples of the sample interval
    if  (header_info.("Sample Interval") != 1)
      csv_data(i,1) = i*header_info.("Sample Interval");
    endif
  endfor
  
endfunction
