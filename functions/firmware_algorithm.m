
## Copyright (C) 2019 andre.hachich
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} firmware_model (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: andre.hachich <andre.hachich@JASP38N>
## Created: 2019-11-29

function [isseed] = firmware_algorithm (fmw_parameter, time_array, voltage)
  sampling_time = 0.0006;
  
  time_array = time_array-time_array(1);
  micro_time = 0:sampling_time:time_array(end);
  
  micro_voltage = round(voltage(lookup(time_array, micro_time)) * 1024 / 3.0);

  
  
  start_usefulwindow_idx = round((1-fmw_parameter.default_jan/100) * length(micro_voltage) / 2);
  end_usefulwindow_idx = length(micro_voltage) - start_usefulwindow_idx;
  middle_usefulwindow_idx = round(start_usefulwindow_idx + (end_usefulwindow_idx - start_usefulwindow_idx)/2);
  
  wave_parameter = struct(...
    "min_jan",        min(micro_voltage(start_usefulwindow_idx:end_usefulwindow_idx)),... ##VALOR MINIMO DO SINAL DA SEM NA JANELA
##    "count_f_mod",    1024,... ##VALOR COM PORC_QUEDA APLICADO P/ VERIFICAR POSICAO DA QUEDA DO SINAL DA SEM NA JANELA
    "count_f",        (end_usefulwindow_idx - start_usefulwindow_idx),... ##VALOR TOTAL DE AMOSTRAS NA JANELA
##    "count_max_pos",  1024,... ##POSICAO DA 1a QUEDA DO SINAL DA SEMENTE NA JANELA EM AMOSTRAS
    "min_trans_s",    min(micro_voltage(1:start_usefulwindow_idx)),... ##MINIMO AD DO VALOR DO SINAL DA SEMENTE TRANSMITIDO VIA STATUS
    "max_1",          max(micro_voltage(start_usefulwindow_idx:middle_usefulwindow_idx)),... ##VALOR DO MAX DO SINAL DA SEMENTE NA JANELA NO 1o QUADRANTE
    "max_2",          max(micro_voltage(middle_usefulwindow_idx:end_usefulwindow_idx)),... ##VALOR DO MAX DO SINAL DA SEMENTE NA JANELA NO 2o QUADRANTE
    "min_total_trig", 1024,... ##MINIMO SINAL DA SEMENTE NO PONTO TRIGGER (INICIO DA JANELA)
##    "max_geral_mod",  1024,... ##75%(MAX_QUAD%) DE MAX_GERAL
    "max_geral",      max(micro_voltage(start_usefulwindow_idx:end_usefulwindow_idx)),... ##MAXIMO GERAL DO SINAL DA SMENTE NA JANELA
    "count_larg_high",sum(micro_voltage(start_usefulwindow_idx:end_usefulwindow_idx) > 300),... ##LARGURA HIGH DO SINAL DA SEMENTE (THR = 300 fixo)
    "deb_patamar",    floor((end_usefulwindow_idx - start_usefulwindow_idx) / 80)...
  );
  
  wave_parameter.max_geral_mod = round(fmw_parameter.max_quad * wave_parameter.max_geral/100);
  wave_parameter.count_f_mod = round(fmw_parameter.porc_queda * wave_parameter.count_f/100);
  [~, wave_parameter.count_max_pos] = max(micro_voltage(start_usefulwindow_idx:end_usefulwindow_idx));
  wave_parameter.count_max_pos += start_usefulwindow_idx + wave_parameter.deb_patamar;
  disp(fmw_parameter)
  disp(wave_parameter)
  
  
##  
##  for i = 1:length(micro_voltage)
##  
##    if i >= start_idx  
##      
##    else
##      if (micro_voltage(i) < parameters.min_jan)
##        parameters.min_jan = micro_voltage(i);
##      endif
##    endif
##    
##  endfor
##  
%  plot(micro_time, micro_voltage, ".b");
%  waitfor(gcf);
  %% Algorithm to know if wave is seed or hole
  
  
  %Calculating parameters:
##  max_geral_mod = round(max_geral * max_quad / 100);  
  thr_min = 400;
  
  

  
  % Status = 0: hole, Status = 1: seed
  status = zeros(8,1);

  
##STATUS 1: COMPARA LARGURA DO SINAL SEMENTE COM X%(60% def) LARGURA SINAL FURO
##  status(1) = !(parameters.count_larg_high > parameters.val_larg_furo);  
####STATUS 2: VERIFICA SE HOUVE VARIAS SUBIDAS E DESCIDAS NO SINAL
##  status(2) = 1;
####STATUS 3:  VERIFICA SE MAX DO PRIMEIRO QUADRANTE > X%(75%def) DO MAXIMO DO SINAL DA SEM NA JANELA
##  status(3) = !(parameters.max_1 > parameters.max_geral_mod);
####STATUS 4:  VERIFICA SE MAX DO SEGUNDO QUADRANTE > X%(75%def) DO MAXIMO DO SINAL DA SEM NA JANELA
##  status(4) = !(parameters.max_2 > parameters.max_geral_mod);
####STATUS 5: SE MAXIMO DA SEMENTE ACIMA DE VAL MIN ABSOLUTO
##  status(5) = !(parameters.max_geral > parameters.thr_min);
####STATUS 6:  VERIFICA SE MIN DO SINAL DA JANELA ESTA ABAIXO DO MINIMO DO PONTO TRIGGER - DELTA
##  status(6) = !((parameters.min_total_trig - parameters.dif_min) <= parameters.min_jan);
####STATUS 7: VERIFICA SE MAX DO SINAL DA JANELA ACIMA DO MINIMO DO PONTO DE TRIGGER + DELTA
##  status(7) = !(parameters.max_geral > (parameters.min_total_trig + parameters.dif_max_falha));
####STATUS 8: VERIFICA SE POSX QUEDA DO SINAL SEM NA JANELA OCORREU ANTES DE X% (60 ou 40% def) DA LARGURA JANELA
##  status(8) = !(parameters.count_max_pos >= parameters.count_f_mod);
##  isseed = !any(status);
  
  isseed = !any(micro_voltage > thr_min);
endfunction


