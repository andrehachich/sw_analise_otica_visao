clc
clear -x "optical_sensor_struct"
close all
more off

addpath("./functions/")
addpath("./scripts/")

if (exist("GlobalVariables.mat"))
  disp("Loading global variables...")
  load "GlobalVariables.mat"
  disp("Done");
endif


function update_plot (obj)
  hmain = guidata (obj);
  
  global visumtitanium_list;
  global opticalsensor_list;
  global disc_list;
  global ring_list;
  global seedpresence_list;
  global neatness_list;
  global lenstransparency_list;
  global optical_sensor_struct;
  
  %Get states of all checkboxes:
  vtit_sel = find(cell2mat(get(hmain.visumtitanium_checkbox,    "value")))';
  sens_sel = find(cell2mat(get(hmain.opticalsensor_checkbox,    "value")))';
  disc_sel = find(cell2mat(get(hmain.disc_checkbox,             "value")))';
  ring_sel = find(cell2mat(get(hmain.ring_checkbox,             "value")))';
  seed_sel = find(cell2mat(get(hmain.seedpresence_checkbox,     "value")))';
  neat_sel = find(cell2mat(get(hmain.neatness_checkbox,         "value")))';
  lens_sel = find(cell2mat(get(hmain.lenstransparency_checkbox, "value")))';

  switch (gcbo)
    case {hmain.updatedatabase_pushbutton}
      set(hmain.updatedatabase_label, "string", "Loading database...");
      update_database
      set(hmain.updatedatabase_label, "string", "Database loaded.");
      
      
    case {hmain.addwave_pushbutton}
      set(hmain.addwave_label, "string", "Select file...")
      %% Choose file from list
      filename = uigetfile("*.csv");
      if (filename != 0)
        set(hmain.addwave_label, "string", "Checking wave...")
        addwave_gui;
        waitfor(gcf);
        set(hmain.addwave_label, "string", "Done.")
      else
        set(hmain.addwave_label, "string", "Invalid file")
      endif %valid chosen file
      
    case {hmain.analyse_pushbutton}
##      analysis_efficiency;
       sorgo_white_comparison
      
      
      
    case {hmain.selectall_checkbox}
      set(hmain.visumtitanium_checkbox,    "value", get(hmain.selectall_checkbox, "value"));
      set(hmain.opticalsensor_checkbox,    "value", get(hmain.selectall_checkbox, "value"));
      set(hmain.disc_checkbox,             "value", get(hmain.selectall_checkbox, "value"));
      set(hmain.ring_checkbox,             "value", get(hmain.selectall_checkbox, "value"));
      set(hmain.seedpresence_checkbox,     "value", get(hmain.selectall_checkbox, "value"));
      set(hmain.neatness_checkbox,         "value", get(hmain.selectall_checkbox, "value"));
      set(hmain.lenstransparency_checkbox, "value", get(hmain.selectall_checkbox, "value"));
  endswitch
endfunction


hmain.fig = figure('units','normalized','position',[0.1 0.1 0.8 0.8]);

button_width = 0.2;
button_height = 0.05;
button_xposition = [0.1 0.4 0.7];
button_yposition = 0.9;

hmain.updatedatabase_label = uicontrol ("style", "text",
                                "units", "normalized",
                                "string", "Empty database",
                                "horizontalalignment", "center",
                                "position", [button_xposition(1) (button_yposition + 0.05) button_width button_height]);
                                
hmain.updatedatabase_pushbutton = uicontrol ("style", "pushbutton",
                                "units", "normalized",
                                "string", "Update Database",
                                "callback", @update_plot,
                                "position", [button_xposition(1) button_yposition button_width button_height]);
                                
                                
hmain.addwave_label = uicontrol ("style", "text",
                                "units", "normalized",
                                "string", "No Wave Selected",
                                "horizontalalignment", "center",
                                "position", [button_xposition(2) (button_yposition + 0.05) button_width button_height]);
hmain.addwave_pushbutton = uicontrol ("style", "pushbutton",
                                "units", "normalized",
                                "string", "Add wave",
                                "callback", @update_plot,
                                "position", [button_xposition(2) button_yposition button_width button_height]);
                                
                                
hmain.analyse_pushbutton = uicontrol ("style", "pushbutton",
                                "units", "normalized",
                                "string", "Analyse",
                                "callback", @update_plot,
                                "position", [button_xposition(3) button_yposition button_width button_height]);
  
checkbox_width = 0.1;  
checkbox_height = 0.05;
checkbox_xposition = 0.05:(checkbox_width+0.05):1;
checkbox_yposition = [0.85, 0.65, 0.45, 0.30, 0];


%%% START CHECKBOX DECLARATION
hmain.visumtitanium_label = uicontrol ("style", "text",
                                "units", "normalized",
                                "string", "Visum Titanium",
                                "horizontalalignment", "left",
                                "position", [checkbox_xposition(1) (checkbox_yposition(1)+0.001) checkbox_width checkbox_height]);                                
for vtit_idx = 1:vtit_max                      
  hmain.visumtitanium_checkbox(vtit_idx) = uicontrol("style", "checkbox",
                                                 "units", "normalized",
                                                 "string", visumtitanium_list{vtit_idx},
                                                 "callback", @update_plot,
                                                 "position", [checkbox_xposition(1) (checkbox_yposition(1)-checkbox_height*vtit_idx) checkbox_width checkbox_height]);
endfor
hmain.opticalsensor_label = uicontrol ("style", "text",
                                "units", "normalized",
                                "string", "Sensor Position",
                                "horizontalalignment", "left",
                                "position", [checkbox_xposition(2) (checkbox_yposition(1)+0.001) checkbox_width checkbox_height]); 
for sens_idx = 1:sens_max
  hmain.opticalsensor_checkbox(sens_idx) = uicontrol("style", "checkbox",
                                                 "units", "normalized",
                                                 "string", opticalsensor_list{sens_idx},
                                                 "callback", @update_plot,
                                                 "position", [checkbox_xposition(2) (checkbox_yposition(1)-checkbox_height*sens_idx) checkbox_width checkbox_height]);
endfor
hmain.disc_label = uicontrol ("style", "text",
                                "units", "normalized",
                                "string", "Disc",
                                "horizontalalignment", "left",
                                "position", [checkbox_xposition(3) (checkbox_yposition(1)+0.001) checkbox_width checkbox_height]); 
for disc_idx = 1:disc_max
  if (disc_idx <= floor(disc_max/2))
    hmain.disc_checkbox(disc_idx) = uicontrol("style", "checkbox",
                                                 "units", "normalized",
                                                 "string", disc_list{disc_idx},
                                                 "callback", @update_plot,
                                                 "position", [checkbox_xposition(3) (checkbox_yposition(1)-checkbox_height*disc_idx) 2*checkbox_width checkbox_height]);
  else
    hmain.disc_checkbox(disc_idx) = uicontrol("style", "checkbox",
                                                 "units", "normalized",
                                                 "string", disc_list{disc_idx},
                                                 "callback", @update_plot,
                                                 "position", [checkbox_xposition(4) (checkbox_yposition(1)-checkbox_height*(disc_idx-floor(disc_max/2))) 2*checkbox_width checkbox_height]);    
  endif
endfor
hmain.ring_label = uicontrol ("style", "text",
                                "units", "normalized",
                                "string", "Ring",
                                "horizontalalignment", "left",
                                "position", [checkbox_xposition(5) (checkbox_yposition(1)+0.001) checkbox_width checkbox_height]); 
for ring_idx = 1:ring_max
  hmain.ring_checkbox(ring_idx) = uicontrol("style", "checkbox",
                                                 "units", "normalized",
                                                 "string", ring_list{ring_idx},
                                                 "callback", @update_plot,
                                                 "position", [checkbox_xposition(5) (checkbox_yposition(1)-checkbox_height*ring_idx) checkbox_width checkbox_height]);
endfor

hmain.seed_label = uicontrol ("style", "text",
                                "units", "normalized",
                                "string", "Seed Presence",
                                "horizontalalignment", "left",
                                "position", [checkbox_xposition(2) (checkbox_yposition(2)+0.001) checkbox_width checkbox_height]); 
for seed_idx = 1:seed_max
  hmain.seedpresence_checkbox(seed_idx) = uicontrol("style", "checkbox",
                                                 "units", "normalized",
                                                 "string", seedpresence_list{seed_idx},
                                                 "callback", @update_plot,
                                                 "position", [checkbox_xposition(2) (checkbox_yposition(2)-checkbox_height*seed_idx) checkbox_width checkbox_height]);
endfor

hmain.neat_label = uicontrol ("style", "text",
                                "units", "normalized",
                                "string", "Disc Neatness",
                                "horizontalalignment", "left",
                                "position", [checkbox_xposition(2) (checkbox_yposition(3)+0.001) checkbox_width checkbox_height]); 
for neat_idx = 1:neat_max
  hmain.neatness_checkbox(neat_idx) = uicontrol("style", "checkbox",
                                                 "units", "normalized",
                                                 "string", neatness_list{neat_idx},
                                                 "callback", @update_plot,
                                                 "position", [checkbox_xposition(2) (checkbox_yposition(3)-checkbox_height*neat_idx) checkbox_width checkbox_height]);
endfor

hmain.lens_label = uicontrol ("style", "text",
                                "units", "normalized",
                                "string", "Lens Transparency",
                                "horizontalalignment", "left",
                                "position", [checkbox_xposition(2) (checkbox_yposition(4)+0.001) checkbox_width checkbox_height]); 
for lens_idx = 1:lens_max
  hmain.lenstransparency_checkbox(lens_idx) = uicontrol("style", "checkbox",
                                                 "units", "normalized",
                                                 "string", lenstransparency_list{lens_idx},
                                                 "callback", @update_plot,
                                                 "position", [checkbox_xposition(2) (checkbox_yposition(4)-checkbox_height*lens_idx) checkbox_width checkbox_height]);
endfor

hmain.selectall_checkbox = uicontrol("style", "checkbox",
                                 "units", "normalized",
                                 "string", "Select All",
                                 "callback", @update_plot,
                                 "value", 1,
                                 "position", [checkbox_xposition(1) checkbox_yposition(5) checkbox_width checkbox_height]);

set (gcf, "color", get(0, "defaultuicontrolbackgroundcolor"))
guidata (gcf, hmain)
update_plot (gcf);

if (exist("optical_sensor_struct"))
  set(hmain.updatedatabase_label, "string", "Database loaded.");
elseif (exist("DatabaseStruct.mat"))
  set(hmain.updatedatabase_label, "string", "Loading database....");
  disp("Loading existing database...")
  global optical_sensor_struct;
  load "DatabaseStruct.mat"
  disp("Done");
  set(hmain.updatedatabase_label, "string", "Database loaded.");
endif

