## Copyright (C) 2019 andre.hachich
## 
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {Function File} {@var{retval} =} old_format_to_new (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author: andre.hachich <andre.hachich@JASP38N>
## Created: 2019-11-07

function [] = old_format_to_new (directoryname, starting_idx, ending_idx)
failed_files = [""];
global visumtitanium_list;
global opticalsensor_list;
global disc_list;
global ring_list;
global seedpresence_list;
global neatness_list;
global lenstransparency_list;
more off

seed = strsplit(directoryname,"_"){1};
seed_idx = find(strcmp(seedpresence_list, seed));
if (isempty(seed_idx))
  disp("Seed presence not found")
endif

neat = strsplit(directoryname,"_"){2};
neat_idx = find(strcmp(neatness_list, neat));
if (isempty(neat_idx))
  disp("Neatness not found")
endif

filelist = {dir([directoryname,"/csv/"]).name}(3:end);

%If no argument of starting / ending index is input, go through all files
if nargin <= 1
  starting_idx = 1;
  ending_idx = length(filelist);
elseif nargin <= 2
  ending_idx = length(filelist);
endif


for i =  starting_idx:ending_idx
  printf(["\n", num2str(i),"/",num2str(ending_idx), "    ", filelist{i}])
  full_filename = filelist{i};
  filename = strsplit(full_filename,{"_", " ","."});
  vtit = [filename{1},"_", filename{2}];
  vtit_idx = find(strcmp(visumtitanium_list, vtit));
  if (isempty(vtit_idx))
    disp("Titanium not found")
  endif
  
  sens = filename{3};
  sens_idx = find(strcmp(opticalsensor_list, sens));
  if (isempty(sens_idx))
    disp("Sensor not found")
  endif 
  
  
  disc = filename{4};
  if (strcmp(disc, "feijao"))
    disc = "feijaomarromescuro";
    ring = "milhoamarelo";
  elseif (strcmp(disc, "soja"))
    disc = "sojalaranja";
    ring = "sojalaranja";
  elseif (strcmp(disc, "sorgo"))
    disc = "sorgocinza";
    ring = "sorgopreto";
  endif
  
  disc_idx = find(strcmp(disc_list, disc));
  if (isempty(disc_idx))
    disp("Disc not found")
  endif 
  
  ring_idx = find(strcmp(ring_list, ring));
  if (isempty(ring_idx))
    disp("Ring not found")
  endif
  
  lens = "clean";
  lens_idx = find(strcmp(lenstransparency_list, lens));
  if (isempty(lens_idx))
    disp("Lens transparency not found")
  endif
  
  date = filename{5};
  
  wave_data = read_csv_wave_file([directoryname,"/csv/",full_filename]);
  time_array = wave_data(:,1);
  
  count_wave = 0;
  entry_index = [1];
  
  % Moving average for the graph.
  for j = 51:length(time_array)
    current_smooth(j) = mean(wave_data(j-50:j, 2));
%    if (sens_idx == 1)
%      current_smooth(j) = mean(w(j-50:j));
%    elseif (sens_idx == 2)
%      current_smooth(j) = mean(current_hole(j-50:j));
%    endif
  endfor
  
  min_threshold = 1.15*mean(current_smooth);
  for j = 51:length(time_array)
    if ((current_smooth(j-1) > min_threshold) && (current_smooth(j) < min_threshold))
      count_wave = count_wave + 1;
      entry_index = [entry_index; j];
      [last_max(count_wave), max_index(count_wave)] = max(current_smooth(entry_index(count_wave):entry_index(count_wave+1)));
      max_index(count_wave) = max_index(count_wave) + entry_index(count_wave);
    endif
  endfor
  
  
  wave_window_time = time_array(max_index(2:end))';
  seed_vector = zeros(1,length(wave_window_time)-1);

  gui_run;
  waitfor(gcf);
  
  clear h
  delete(gcf);
  clear time_array current_smooth wave_window_time end_time seed_vector max_index entry_index count_wave last_max current_seed current_hole
endfor


endfunction
