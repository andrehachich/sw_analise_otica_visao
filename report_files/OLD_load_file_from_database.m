%%% File structure %%%
%Folder name = "./"+ titanium_analysis + "/" + optical_sensor_analysis + "/csv/" 
%File name = disc_ring_seedpresence_cleanness_lenstransparency_yyyymmdd


for index_dirt = 1:index_dirt_max  
  %List file names in the directory relative to the disc type
  %strtrim guarantees no whitespace around the string due to different string sizes in array.
  csv_directoryname = ["./",strtrim(dirt_analysis(index_dirt,:)), "/csv/"];
  foto_directoryname = ["./",strtrim(dirt_analysis(index_dirt,:)), "/fotos/"];
  filelist = {dir(csv_directoryname).name};
  
  for index_titanium = 1:index_titanium_max
    for index_optical = 1:index_optical_max
%      disp(["Including files to ",...
%            strtrim((titanium_analysis(index_titanium,:))),"_",...
%            strtrim((optical_sensor_analysis(index_optical,:)))]);
      for index_disc = 1:index_disc_max
        
        %Build filename from the chosen characteristics to analyse.
        if (isempty(strfind(dirt_analysis(index_dirt,:),"white")))
          filename = [strtrim(titanium_analysis(index_titanium,:)),"_",...
                      strtrim(optical_sensor_analysis(index_optical,:)),"_",...
                      strtrim(disc_analysis(index_disc,:))];
        else
          %If searching into "white" directory, there is no disc associated.
          filename = [strtrim(titanium_analysis(index_titanium,:)),"_",...
                      strtrim(optical_sensor_analysis(index_optical,:))];  
        endif  
        
        
        for i = 1:length(filelist)
          %Find file whose name starts with the filename chosen
          if (!isempty(strfind(filelist{i},filename)))
            filename = filelist{i};
            
            [date, time, current] = read_csv_file([csv_directoryname,filename]);

            
            
            %Fill wave struct with data from current file
            wave_struct(index_disc + (index_dirt-1)*index_disc_max, index_optical + (index_titanium-1)*index_optical_max).("File Name") = filename;
            wave_struct(index_disc + (index_dirt-1)*index_disc_max, index_optical + (index_titanium-1)*index_optical_max).("Disc") = disc_analysis(index_disc,:);
            wave_struct(index_disc + (index_dirt-1)*index_disc_max, index_optical + (index_titanium-1)*index_optical_max).("Time") = time;
            
            if strcmp(strtrim(optical_sensor_analysis(index_optical,:)), "sem")
              wave_struct(index_disc + (index_dirt-1)*index_disc_max, index_optical + (index_titanium-1)*index_optical_max).("Current Seed Sensor") = current;
            else
              wave_struct(index_disc + (index_dirt-1)*index_disc_max, index_optical + (index_titanium-1)*index_optical_max).("Current Hole Sensor") = current;
            endif
            
            
          endif          
        endfor
      endfor
      
      optical_sensor_struct(index_optical + (index_titanium-1)*index_optical_max).("Wave Database") = ...
        wave_struct(:, index_optical + (index_titanium-1)*index_optical_max);
    endfor
  endfor
endfor
