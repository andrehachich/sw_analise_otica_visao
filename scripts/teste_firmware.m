  voltage(1:8) = 1.0283;
  voltage(9:96) = ...
  [1.0283
  0.8936
  0.7676
  0.6533
  0.5479
  0.4541
  0.3750
  0.3047
  0.2549
  0.2432
  0.2402
  0.2373
  0.2344
  0.2314
  0.2314
  0.2285
  0.2256
  0.2256
  0.2227
  0.2197
  0.2168
  0.2168
  0.2139
  0.2139
  0.2139
  0.2139
  0.2168
  0.2139
  0.2139
  0.2168
  0.2168
  0.2197
  0.2197
  0.2256
  0.2256
  0.2285
  0.2373
  0.2432
  0.2578
  0.2783
  0.3105
  0.3574
  0.4248
  0.5127
  0.7354
  0.8730
  1.0078
  1.1367
  1.2539
  1.3652
  1.5703
  1.6318
  1.7021
  1.7754
  1.8398
  1.9072
  1.9688
  2.0303
  2.0830
  2.1357
  2.1797
  2.2236
  2.2588
  2.2881
  2.3174
  2.3379
  2.3613
  2.3789
  2.3936
  2.4111
  2.4229
  2.4404
  2.4492
  2.4609
  2.4697
  2.4785
  2.4902
  2.4961
  2.5049
  2.5107
  2.5195
  2.5283
  2.5313
  2.5400
  2.5400
  2.5459
  2.5459
  2.5459];
  voltage(97:104) = 2.5459;
  fmw_parameter = struct(...
    "max_quad",       75,...
    "thr_min",        400,... ##VALOR MINIMO ABSOLUTO A SER COMPARADO AO MAX DO SINAL SEM NA JANELA
    "dif_max_falha",  150,... ##DELTA A SOMAR DO MINIMO DO PONTO DE TRIGGER
    "porc_queda",     60,... ##PORCENTAGEM A SER APLICADO NO PERIODO DA JANELA P/ VERIFICACAO DE POSICAO DE QUEDA DO SINAL DA SEM NA JANELA
    "dif_min",        100,... ##DELTA A SUBTRAIR DO MINIMO DO PONTO DE TRIGGER
    "val_larg_furo",  27,...  ##60%(DEFAULT) do VALOR DO HIGH (c/THR = 50%) DO FURO
    "default_jan",    85 ... ## porcentagem da janela �til
  );
  
  
  time_array = 0:0.0006:((length(voltage)-1)*0.0006);
  
##  isseed = firmware_algorithm(fmw_parameter, time_array, voltage);