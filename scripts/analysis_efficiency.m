clc
pkg load signal

%% Check if selection vector is set, if not select all
if (!exist("vtit_sel")) vtit_sel = 1:vtit_max; endif
if (!exist("sens_sel")) sens_sel = 1:sens_max; endif
sens_sel = 3;
if (!exist("disc_sel")) disc_sel = 1:disc_max; endif
if (!exist("ring_sel")) ring_sel = 1:ring_max; endif
if (!exist("seed_sel")) seed_sel = 1:seed_max; endif
if (!exist("neat_sel")) neat_sel = 1:neat_max; endif
if (!exist("lens_sel")) lens_sel = 1:lens_max; endif

vtit_resistor_idx = [5 5 5 4 1 4 4 2 1 4];
##phototrans_resistor = [166, 208, 255, 297, 374, 470, 806] * 1000; %kOhm, as defined by hardware rev. k Visum Titanium
##phrs_idx = 1;
##phrs_max = length(phototrans_resistor);
##%phrs_sel = 1:phrs_max;
phrs_sel = 2;

wave_count = zeros(size(vtit_sel));
success_count = zeros(size(vtit_sel));
false_seed = zeros(size(vtit_sel));
false_hole = zeros(size(vtit_sel));

hole_count = zeros(size(vtit_sel));
seed_count = zeros(size(vtit_sel));


for vtit_idx = vtit_sel
##  disp(["Starting analysis of Visum Titanium ", num2str(vtit_idx), " out of ", num2str(length(vtit_sel))]);
  for sens_idx = sens_sel
    
    wave_max = length(optical_sensor_struct(vtit_idx, sens_idx).wave_database);
    for wave_idx = 1:wave_max
    
      %Find if wave is in selection
      if (any(cell2mat(strfind(optical_sensor_struct(vtit_idx, sens_idx).wave_database(wave_idx).disc,             disc_list(disc_sel)))) &&...
          any(cell2mat(strfind(optical_sensor_struct(vtit_idx, sens_idx).wave_database(wave_idx).ring,             ring_list(ring_sel)))) &&...
          any(cell2mat(strfind(optical_sensor_struct(vtit_idx, sens_idx).wave_database(wave_idx).seedpresence,     seedpresence_list(seed_sel)))) &&...
          any(cell2mat(strfind(optical_sensor_struct(vtit_idx, sens_idx).wave_database(wave_idx).neatness,         neatness_list(neat_sel)))) &&...
          any(cell2mat(strfind(optical_sensor_struct(vtit_idx, sens_idx).wave_database(wave_idx).lenstransparency, lenstransparency_list(lens_sel)))))
        %Flag to plot wave if there is an error
        plot_wave = 0;
        
        for phrs_idx = phrs_sel
          
##          if (optical_sensor_struct(vtit_idx,1).wave_database(wave_idx).hole_resistor == 0)
##            optical_sensor_struct(vtit_idx,1).wave_database(wave_idx).hole_resistor = optical_sensor_struct(vtit_idx,2).wave_database(wave_idx).hole_resistor;
##            optical_sensor_struct(vtit_idx,1).wave_database(wave_idx).hole_voltage = optical_sensor_struct(vtit_idx,2).wave_database(wave_idx).hole_voltage;
##          endif
##          [optical_sensor_struct(vtit_idx,1).wave_database(wave_idx).seed_voltage, ~, ~] =...
##                 hardware_model(optical_sensor_struct(vtit_idx, 1).wave_database(wave_idx));
##                                                                  phototrans_resistor(vtit_resistor_idx(vtit_idx)));

##          if (wave_idx <= length(optical_sensor_struct(vtit_idx, 2).wave_database))
##            optical_sensor_struct(vtit_idx,2).wave_database(wave_idx).seed_resistor = optical_sensor_struct(vtit_idx,1).seed_resistor;
##            optical_sensor_struct(vtit_idx,2).wave_database(wave_idx).hole_resistor = optical_sensor_struct(vtit_idx,2).hole_resistor;
##            
##            [~, optical_sensor_struct(vtit_idx,1).wave_database(wave_idx).hole_voltage, ~] =...
##                   hardware_model(optical_sensor_struct(vtit_idx, 2).wave_database(wave_idx));
##                                                                  phototrans_resistor(vtit_resistor_idx(vtit_idx))); 
##          endif;                                                      
          isseed = [];
##          figure
##          plot(optical_sensor_struct(vtit_idx, sens_idx).wave_database(wave_idx).hole_voltage);
##          hold on
##          plot(optical_sensor_struct(vtit_idx, sens_idx).wave_database(wave_idx).seed_voltage, "r");
##          waitfor(gcf);
          
          parameters = firmware_parameters_estimation(optical_sensor_struct(vtit_idx, sens_idx).wave_database(wave_idx));
          wave_count(vtit_idx) += length(optical_sensor_struct(vtit_idx, sens_idx).wave_database(wave_idx).seed_vector);          
          seed_count(vtit_idx) += sum(optical_sensor_struct(vtit_idx, sens_idx).wave_database(wave_idx).seed_vector);
          hole_count(vtit_idx) += sum(!optical_sensor_struct(vtit_idx, sens_idx).wave_database(wave_idx).seed_vector);

          for svec_idx = 1:length(optical_sensor_struct(vtit_idx, sens_idx).wave_database(wave_idx).seed_vector)
            
            time_array = optical_sensor_struct(vtit_idx, sens_idx).wave_database(wave_idx).time_array;
            start_idx = lookup(time_array, optical_sensor_struct(vtit_idx, sens_idx).wave_database(wave_idx).wave_window_time(svec_idx));
            end_idx = lookup(time_array, optical_sensor_struct(vtit_idx, sens_idx).wave_database(wave_idx).wave_window_time(svec_idx+1));
            
            isseed(svec_idx) = firmware_algorithm(parameters, time_array(start_idx:end_idx),...
                                                  optical_sensor_struct(vtit_idx, sens_idx).wave_database(wave_idx).seed_voltage(start_idx:end_idx));

            %Statistical analysis
            
            if (isseed(svec_idx) == optical_sensor_struct(vtit_idx, sens_idx).wave_database(wave_idx).seed_vector(svec_idx))
              success_count(vtit_idx) += 1;
            elseif (isseed(svec_idx) == 1)
              false_seed(vtit_idx) += 1;
%              plot_wave = 1;
            else
              false_hole(vtit_idx) += 1;
%              plot_wave = 1;
            endif
            
          endfor %wave in seed vector
          
        endfor %photoresistor
        if (plot_wave)
          figure;
          plot(time_array, wave_seed_V);
          hold on
          plot(optical_sensor_struct(vtit_idx, sens_idx).wave_database(wave_idx).wave_window_time, 0.2, ".k")
          title([disc_list{disc_idx}, " ", ring_list{ring_idx}, " ", neatness_list{neat_idx}, " resistor ", num2str(phototrans_resistor(vtit_resistor_idx(vtit_idx))), "\n"...
                 "Gabarito: ", num2str(optical_sensor_struct(vtit_idx, sens_idx).wave_database(wave_idx).seed_vector),...
                 "\nAlgoritmo: ", num2str(isseed)]);
          waitfor(gcf);
        endif
      endif % wave is in selection
    endfor % waves in wave file
      
      
    
  endfor %sens
  
  
  
  
  disp([visumtitanium_list{vtit_idx}, " evaluation (", num2str(vtit_idx), "/", num2str(length(vtit_sel)), "):"])
  disp(["   Success rate: " num2str(success_count(vtit_idx)), "/", num2str(wave_count(vtit_idx)), " total waves"...
         "  ", num2str(success_count(vtit_idx)*100/wave_count(vtit_idx)), "%"]);
         
  disp(["   False seed  : " num2str(false_seed(vtit_idx)), "/", num2str(hole_count(vtit_idx)), " total holes",...
         "  ", num2str(false_seed(vtit_idx)*100/hole_count(vtit_idx)), "%"]);
         
  disp(["   False hole  : " num2str(false_hole(vtit_idx)), "/", num2str(seed_count(vtit_idx)), " total seeds"...
         "  ", num2str(false_hole(vtit_idx)*100/seed_count(vtit_idx)), "%"]);   

endfor %vtit