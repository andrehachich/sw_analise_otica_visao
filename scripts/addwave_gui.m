clear h

graphics_toolkit qt


filename = strrep(filename, ".csv","");
printf(["\nAdding file: ", filename]);
set(hmain.addwave_label, "string", "Adding file...")
date = strsplit(filename, {"_", " ","."}){end};
wave_data = read_csv_wave_file([filename, ".csv"]);
time_array = wave_data(:,1);
wave_smooth = zeros(size(wave_data, 1), size(wave_data,2)-1);

moving_avg_size = 50;
% Apply moving average to data
for data_idx = 1:size(wave_data, 1)
  for wave_idx = 2:size(wave_data,2)
    wave_smooth(data_idx,wave_idx-1) = mean(wave_data(max(1, data_idx-moving_avg_size):data_idx, wave_idx));
  endfor
endfor

% Auto detect maximum points of waves
for wave_idx = 1:size(wave_smooth,2)
  min_threshold(wave_idx) = 1.15*mean(wave_smooth(:,wave_idx));
  count_wave(wave_idx) = 0;
  entry_index(:,wave_idx) = [1];
  max_index(:, wave_idx) = [1];
  
  for data_idx = 2:size(wave_smooth, 1)
    if ((wave_smooth(data_idx-1,wave_idx) > min_threshold(wave_idx))...
         && (wave_smooth(data_idx, wave_idx) < min_threshold(wave_idx)))
         
      count_wave(wave_idx) = count_wave(wave_idx) + 1;
      entry_index(length(entry_index)+1,wave_idx) = data_idx;
      
      [~, max_index(count_wave(wave_idx), wave_idx)] = max(wave_smooth(entry_index(count_wave(wave_idx),   wave_idx):...
                                                                       entry_index(count_wave(wave_idx)+1, wave_idx)));
      max_index(count_wave(wave_idx), wave_idx) = max_index(count_wave(wave_idx)) + entry_index(count_wave(wave_idx), (wave_idx));
    endif
  endfor
  wave_window_time(:, wave_idx) = time_array(max_index(2:end, wave_idx));
  seed_vector(:, wave_idx) = zeros(1,length(wave_window_time)-1);
endfor


function update_waveplot (obj)

  ## gcbo holds the handle of the control
  h = guidata (obj);
  
  replot = false;
  
  
  vtit_list = get(h.vtit_popup, "string");
  vtit = vtit_list{get(h.vtit_popup, "value")};
  
  sens_list = get(h.sens_popup, "string");
  sens = sens_list{get(h.sens_popup, "value")};
  
  disc_list = get(h.disc_popup, "string");
  disc = disc_list{get(h.disc_popup, "value")};
  
  ring_list = get(h.ring_popup, "string");
  ring = ring_list{get(h.ring_popup, "value")};
  
  seed_list = get (h.seed_popup, "string");
  seed = seed_list{get(h.seed_popup, "value")};
  
  neat_list = get(h.neat_popup, "string");
  neat = neat_list{get(h.neat_popup, "value")};
  
  lens_list = get(h.lens_popup, "string");
  lens = lens_list{get(h.lens_popup, "value")};
  
  date = h.date;
  
  seed_vector = str2num (get (h.seedvector_edit, "string"));
  
  wave_window_time = str2num (get (h.wavewindow_edit, "string"));
  
  time_array = get (h.plot_fullwave, "xdata");
  
  wave_smooth = get (h.plot_fullwave, "ydata");
  
  set (h.save_label, "string", ["Save file: ", h.filename, "\n"...
                                "As: ./database/", vtit, "/", sens,"/", disc,"_",ring,"_",seed,"_",neat,"_",lens," ", h.date]);
  
  
  switch (gcbo)
    case {h.seedvector_edit}
       replot = true;
    case {h.wavewindow_edit}
       replot = true;
    case {h.seed_popup}
      v = get (h.seed_popup, "value");
      seed_vector = str2num(get (h.seedvector_edit, "string"));
      if (v == 1)
        seed_vector = zeros(size(seed_vector));
      elseif (v == 3)
        seed_vector = ones(size(seed_vector));
      endif
      set (h.seedvector_edit, "string", ["[",num2str(seed_vector),"]"]);

      replot = true;
    
    case {h.save_pushbutton}
      if (length(seed_vector) != (length(wave_window_time)-1))
        set (h.save_label, "string", ["Seed vector length(", num2str(length(seed_vector)), ") ",...
              "has to be one fewer than Wave Window length(", num2str(length(wave_window_time)), ")"]);
      else
        set (h.save_label, "string", "Saving...")      
        printf("     Saving.")
        info_filename = ["infowave_", h.filename, ".csv"];    
        output_string = ["Date;",                                 date, "\n",...
                         "Wave Window Time;[",                    num2str(wave_window_time),"]\n",...                    
                         "Seed Vector;[",                         num2str(seed_vector),"]\n"...
                         "Power Supply Voltage;",                 "3\n",...
                         "Seed PhotoTransistor Resistor;",        "0\n",...
                         "Hole PhotoTransistor Resistor;",        "0\n",...
                         "Disc;",                                 disc, "\n",...
                         "Ring;",                                 ring, "\n",...
                         "Seed Presence;",                        seed, "\n",...
                         "Neatness;",                             neat, "\n",...
                         "Lens Transparency;",                    lens, "\n"];
        save(info_filename, "output_string");
        printf(".")
%        copyfile([h.filename, ".csv"],["./database/",h.full_filename]);
%        copyfile([h.filename, ".jpeg"],["./database/",strrep(h.full_filename,".csv",".jpeg")]);
        printf(".")
        add_wave_to_database (h.filename, vtit, sens, disc, ring, seed, neat, lens, date);
        printf(" Success\n")
        close
      endif
      
    case [{h.plot_fullwave}; {h.plot_validwave}]
      user_point = (get(h.ax, "CurrentPoint"))(1,1:2);
      
      % Find point in x closer to user point
      [~, user_index(1)] = min(abs(time_array-user_point(1)));
      
      % Define a margin dx by which we will analyze which y value is closer to user point
      dx = 50;
      % Find new user index, closest in y to user point in selected time margin
      [~, user_index(2)] = min(abs(wave_smooth((user_index(1)-dx):(user_index(1)+dx))-user_point(2)));
      user_index(2) = user_index(1) + user_index(2) - (dx + 1);

      %If user selects a point on the graph, create a new wave_window_point there and associate it with seed_vector = 0      
      wave_window_time(end+1) = time_array(user_index(2));
      %Order wave_window_time
      wave_window_time = sort(wave_window_time);
      %Find index of new inserted point in wave window time sorted array
      [~, user_index(3)] = min(abs(wave_window_time-time_array(user_index(2))));
      
      % If user selects point after the last element wave_window_time, just insert a zero after the last element of seed_vector 
      if (user_index(3) == length(wave_window_time))
        seed_vector(end+1) = 0;
      else
        %Insert a 0 in the seed_vector in the specified position
        seed_vector = [seed_vector(1:user_index(3)-1), 0, seed_vector(user_index(3):end)];
      endif
      replot = true;
      
      
    case {h.plot_maxpoints}
      user_point = (get(h.ax, "CurrentPoint"))(1,1:2);
      [~, user_index] = min(abs(wave_window_time-user_point(1))); 
      
      %Wave window time has one position more than seed vector.
      if (user_index == length(wave_window_time))
        % If user selects last point on wave_window_time, just delete that point and the last seed_vector element
        wave_window_time(user_index) = [];
        seed_vector(end) = [];
      else
        if (seed_vector(user_index) == 0)
          % If selected point was 0 on seed vector, change it to 1  
          seed_vector(user_index) = 1;
        else
          % If selected point is 1, delete it from seed vector and wave window time
          seed_vector(user_index) = [];
          wave_window_time(user_index) = [];  
        endif
      endif
      replot = true;
      
  endswitch

  if (replot)
     
    for i = 1:length(wave_window_time)
      [~, wave_window_index(i)] = min(abs(time_array - wave_window_time(i))); 
    endfor 
     
     
     set (h.plot_maxpoints, "xdata", wave_window_time);
     set (h.plot_maxpoints, "ydata", wave_smooth(wave_window_index));
     
     set (h.plot_validwave, "xdata", time_array(wave_window_index(1):wave_window_index(end)));
     set (h.plot_validwave, "ydata", wave_smooth(wave_window_index(1):wave_window_index(end)));
     
     % Set seed_vector and wave_window_time textboxes with seed_vector and wave_window_time
     set (h.seedvector_edit, "string", ["[",num2str(seed_vector),"]"]);
     set (h.wavewindow_edit, "string",["[",strrep(num2str(wave_window_time), "      ", " "),"]"])

     %For all the points in the seed vector
     for i = 1:max(length(seed_vector), length(h.plot_seedvector))
        if i>length(seed_vector)
          set (h.plot_seedvector(i), "string", "");
          
        elseif i>length(h.plot_seedvector)
          %Find index of time_array closer to wave_window_time
          [~, wave_window_index] = min(abs(time_array-wave_window_time(min(i, length(wave_window_time)))));
          h.plot_seedvector(i) = text(wave_window_time(min(i, length(wave_window_time))),...
%                                      current_smooth(lookup(time_array, wave_window_time)(min(i, length(wave_window_time)))),...
                                      wave_smooth(wave_window_index),...
                                      strsplit(num2str(seed_vector), " ")(i));
        
        else
          [~, wave_window_index] = min(abs(time_array-wave_window_time(min(i, length(wave_window_time)))));
          set (h.plot_seedvector(i), "position", [wave_window_time(min(i, length(wave_window_time))),...
%                                                  current_smooth(lookup(time_array, wave_window_time)(min(i, length(wave_window_time))))],...
                                                  wave_smooth(wave_window_index)],...
                                     "string", strsplit(num2str(seed_vector)," ")(i));
        endif
        
     endfor     
     drawnow();
  endif
  
  
endfunction

%% Initialization of graphic tools
## Graphic axes position
h.fig = figure;
h.ax = axes ("position", [0.10 0.45 0.5 0.5]);
## Seed Vector
h.seedvector_label = uicontrol ("style", "text",
                                "units", "normalized",
                                "string", "Seed Vector",
                                "horizontalalignment", "left",
                                "position", [0.65 0.85 0.30 0.08]);

for wave_idx = 1:size(wave_smooth, 2)
  h.seedvector_edit = uicontrol ("style", "edit",
                               "units", "normalized",
                               "string", ["[",num2str(seed_vector(:,wave_idx)'),"]"],
                               "callback", @update_waveplot,
                               "position", [0.65 (0.80-0.1*(wave_idx-1)) 0.30 0.06]);
endfor
                               
h.wavewindow_label = uicontrol ("style", "text",
                                "units", "normalized",
                                "string", "Wave Window Time",
                                "horizontalalignment", "left",
                                "position", [0.65 0.70 0.30 0.08]);
for wave_idx = 1:size(wave_smooth, 2)
  h.wavewindow_edit = uicontrol ("style", "edit",
                               "units", "normalized",
                               "string", ["[",strrep(num2str(wave_window_time(:,wave_idx)'), "      ", " "),"]"],
                               "callback", @update_waveplot,
                               "position", [0.65 (0.65-0.1*(wave_idx-1)) 0.30 0.06]);
endfor




## Visum Titanium
h.vtit_label = uicontrol ("style", "text",
                               "units", "normalized",
                               "string", "Visum Titanium:",
                               "horizontalalignment", "left",
                               "position", [0.05 0.25 0.35 0.08]);
h.vtit_popup = uicontrol ("style", "popupmenu",
                               "units", "normalized",
                               "string", visumtitanium_list(vtit_sel),
                               "callback", @update_waveplot,
%                               "value", vtit_idx,
                               "position", [0.05 0.20 0.15 0.06]);
                               
## Sensor position
h.sens_label = uicontrol ("style", "text",
                               "units", "normalized",
                               "string", "Optical Sensor:",
                               "horizontalalignment", "left",
                               "position", [0.25 0.25 0.6 0.08]);
h.sens_popup = uicontrol ("style", "popupmenu",
                               "units", "normalized",
                               "string", opticalsensor_list(sens_sel),
%                               "value", sens_idx,
                               "callback", @update_waveplot,
                               "position", [0.25 0.2 0.15 0.06]);
                               
## Disc
h.disc_label = uicontrol ("style", "text",
                               "units", "normalized",
                               "string", "Seed Disc:",
                               "horizontalalignment", "left",
                               "position", [0.65 0.25 0.35 0.08]);
h.disc_popup = uicontrol ("style", "popupmenu",
                               "units", "normalized",
                               "string", disc_list(disc_sel),
%                               "value", disc_idx,
                               "callback", @update_waveplot,
                               "position", [0.65 0.2 0.30 0.06]);
## Ring
h.ring_label = uicontrol ("style", "text",
                               "units", "normalized",
                               "string", "Seed Ring:",
                               "horizontalalignment", "left",
                               "position", [0.65 0.10 0.35 0.08]);
h.ring_popup = uicontrol ("style", "popupmenu",
                               "units", "normalized",
                               "string", ring_list(ring_sel),
%                               "value", ring_idx,
                               "callback", @update_waveplot,
                               "position", [0.65 0.05 0.30 0.06]);
                               
## Seed Presence
h.seed_label = uicontrol ("style", "text",
                               "units", "normalized",
                               "string", "Seed Presence:",
                               "horizontalalignment", "left",
                               "position", [0.05 0.10 0.35 0.08]);
h.seed_popup = uicontrol ("style", "popupmenu",
                               "units", "normalized",
                               "string", seedpresence_list(seed_sel),
%                               "value", seed_idx,
                               "callback", @update_waveplot,
                               "position", [0.05 0.05 0.15 0.06]);
       
## Neatness
h.neat_label = uicontrol ("style", "text",
                               "units", "normalized",
                               "string", "Neatness:",
                               "horizontalalignment", "left",
                               "position", [0.25 0.10 0.35 0.08]);
h.neat_popup = uicontrol ("style", "popupmenu",
                               "units", "normalized",
                               "string", neatness_list(neat_sel),
%                               "value", neat_idx,
                               "callback", @update_waveplot,
                               "position", [0.25 0.05 0.15 0.06]);    

## Lens Transparency
h.lens_label = uicontrol ("style", "text",
                               "units", "normalized",
                               "string", "Lens State:",
                               "horizontalalignment", "left",
                               "position", [0.45 0.10 0.15 0.08]);
h.lens_popup = uicontrol ("style", "popupmenu",
                               "units", "normalized",
                               "string", lenstransparency_list(lens_sel),
%                               "value", lens_idx,
                               "callback", @update_waveplot,
                               "position", [0.45 0.05 0.12 0.06]); 

## save button
h.save_pushbutton = uicontrol ("style", "pushbutton",
                                "units", "normalized",
                                "string", "Save File into database",
                                "callback", @update_waveplot,
                                "position", [0.65 0.45 0.30 0.1]);
h.save_label = uicontrol ("style", "text",
                               "units", "normalized",
                               "horizontalalignment", "left",
                               "position", [0.05 0.30 0.9 0.10]);                               
                               
%% Initialization of graphic plot

for wave_idx = 1:size(wave_smooth,2)
  h.plot_fullwave(wave_idx) = plot(time_array, wave_smooth(:,wave_idx), '--k', "ButtonDownFcn", @update_waveplot);
  hold on
  grid on
  h.plot_validwave(wave_idx) = plot(time_array(max_index(2, wave_idx):max_index(end, wave_idx), wave_idx),...
                                    wave_smooth(max_index(2, wave_idx):max_index(end, wave_idx), wave_idx),...
                                    'b', "ButtonDownFcn", @update_waveplot);
                                    
  h.plot_triggerpoints(wave_idx) = plot(time_array(entry_index(1:end, wave_idx)), wave_smooth(entry_index(1:end, wave_idx), wave_idx), '.g');
  
  h.plot_seedvector(:,wave_idx) = text(wave_window_time(1:end-1, wave_idx),...
                                       wave_smooth(lookup(time_array, wave_window_time(1:end-1, wave_idx)), wave_idx),...
                                       strsplit(num2str(seed_vector(:, wave_idx))," "));                         
 %% Increasing size of h.plot_seedvector in initialization, because on runtime it is not possible                        
  for i = 1:20
    h.plot_seedvector(length(h.plot_seedvector)+1,wave_idx) = text(0,0, "");
  endfor
  
  h.plot_maxpoints(wave_idx) = plot(wave_window_time(:, wave_idx),...
                               wave_smooth(lookup(time_array, wave_window_time(:, wave_idx)), wave_idx),...
                               '.r', "ButtonDownFcn", @update_waveplot);
endfor

%% Initialization of custom data
%h.directoryname = directoryname;
h.filename = filename;
h.date = date;

set (gcf, "color", get(0, "defaultuicontrolbackgroundcolor"))

guidata (gcf, h)
update_waveplot (gcf);