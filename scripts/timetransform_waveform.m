clear all
clc
close all

distortion = 0.5;

filename = "./report_files/OndaPadrao/18,5rpm_semsemente_furo.csv";
wave_data = read_csv_wave_file (filename);
time_array_empty = wave_data(:,1);

for i = 1:length(wave_data)
  hole_empty(i) = mean(wave_data(max(1,i-50):i, 2));
endfor

hole_empty_window = [0.11000    0.25414    0.39829    0.54243    0.68658    0.83072    0.97486];
for i = 1:length(hole_empty_window)
  [~, hole_empty_index(i)] = min(abs(time_array_empty-hole_empty_window(i)));
end

filename = "./report_files/OndaPadrao/18,5rpm_semsemente_semente.csv";
wave_data = read_csv_wave_file (filename);
time_array_empty = wave_data(:,1);
for i = 1:length(wave_data)
  seed_empty(i) = mean(wave_data(max(1,i-50):i, 2));
endfor

seed_empty_window = [0.17000    0.31414    0.45829    0.60243    0.74658    0.89072];
for i = 1:length(seed_empty_window)
  [~, seed_empty_index(i)] = min(abs(time_array_empty-seed_empty_window(i)));
end

wave_length = mean(diff(hole_empty_window));
delay_window = mean(seed_empty_window - hole_empty_window(1:length(seed_empty_window)))/wave_length;

subplot(3,1,1)
plot(time_array_empty, hole_empty);
hold on
plot(hole_empty_window, hole_empty(hole_empty_index), ".k")
plot(time_array_empty, seed_empty-3, "r");
plot(seed_empty_window, seed_empty(seed_empty_index)-3, ".k")

% Generating long vector with valid data
hole_empty = [hole_empty(hole_empty_index(1):hole_empty_index(end))];
%              hole_empty(hole_empty_index(1):hole_empty_index(end))];
%              hole_empty(hole_empty_index(1):hole_empty_index(end));...
%              hole_empty(hole_empty_index(1):hole_empty_index(end))];



seed_empty = [seed_empty(hole_empty_index(1):hole_empty_index(end))];
%              seed_empty(hole_empty_index(1):hole_empty_index(end))];
%              seed_empty(hole_empty_index(1):hole_empty_index(end));...
%              seed_empty(hole_empty_index(1):hole_empty_index(end))];

time_array_empty = time_array_empty(hole_empty_index(1):hole_empty_index(end)) - hole_empty_window(1);
%time_array_empty = [time_array_empty;...
%              time_array_empty+1*(time_array_empty(end)+mean(diff(time_array_empty)));...
%              time_array_empty+2*(time_array_empty(end)+mean(diff(time_array_empty)));...
%              time_array_empty+3*(time_array_empty(end)+mean(diff(time_array_empty)))];
              
seed_empty_window = (seed_empty_window(1) - hole_empty_window(1)):mean(diff(seed_empty_window)):time_array_empty(end);


clear seed_empty_index

for i = 1:length(seed_empty_window)
  [~, seed_empty_index(i)] = min(abs(time_array_empty-seed_empty_window(i)));
end
 
hole_empty_window = 0:mean(diff(hole_empty_window)):time_array_empty(end);
clear hole_empty_index
for i = 1:length(hole_empty_window)
  [~, hole_empty_index(i)] = min(abs(time_array_empty-hole_empty_window(i)));
end

sampling_time = mean(diff(time_array_empty));
dist_wave = [1 (1+distortion) 1 1 (1-distortion) 1];
%dist_wave = [1.00 1.40 0.99 0.75 0.77 0.86]; %Cristalina 1
%dist_wave = [0.97 1.20 1.20 1.09 0.77 0.63]; %Cristalina 2
%dist_wave = [0.77 0.97 1.20 1.40 0.77 0.86]; %Cristalina 3
j = 1;
time_array_dist(1) = time_array_empty(1);
for i = 1:length(time_array_empty)-1
  if j < (length(hole_empty_index)) && i > hole_empty_index(j+1)
    j = j+1;
  endif
  time_array_dist(i+1) = time_array_dist(i) + dist_wave(j)*sampling_time;
endfor

subplot(3,1,2)
plot(time_array_dist, hole_empty);
hold on
plot(time_array_dist(hole_empty_index), hole_empty(hole_empty_index), ".k")
plot(time_array_dist, seed_empty-3, "r");
plot(time_array_dist(seed_empty_index), seed_empty(seed_empty_index)-3, ".k")

hole_adj(1) = hole_empty(1);
seed_adj(1) = seed_empty(1);

time_array_adj = time_array_dist(1):sampling_time:time_array_dist(end);
for i = 1:length(time_array_adj)
  idx = lookup(time_array_dist, time_array_adj(i));
  hole_adj(i) = hole_empty(idx) + (hole_empty(idx+1)-hole_empty(idx))/(time_array_dist(idx+1)-time_array_dist(idx)) * (time_array_adj(i)-time_array_dist(idx));
  seed_adj(i) = seed_empty(idx) + (seed_empty(idx+1)-seed_empty(idx))/(time_array_dist(idx+1)-time_array_dist(idx)) * (time_array_adj(i)-time_array_dist(idx));
  
%  hole_adj(i) = hole_empty(floor(time_array_dist(i)/sampling_time)+1);
%  seed_adj(i) = seed_empty(floor(time_array_dist(i)/sampling_time)+1);
endfor


%for i = 2:length(time_array_empty)
%  for j = 1:length(time_array_dist)
%    if (time_array_dist(j) <= time_array_empty(i) && time_array_dist(j)>= time_array_empty(i-1))
%        hole_dist(i) = hole_empty(j);
%        seed_dist(i) = seed_empty(j);
%        break;
%    endif
%  endfor
%endfor
      
subplot(3,1,3)
plot(time_array_adj, hole_adj);
hold on
%plot(time_array_dist(hole_empty_index), hole_empty(hole_empty_index), ".k")
plot(time_array_adj, seed_adj-3, "r");
%plot(time_array_dist(seed_empty_index), seed_empty(seed_empty_index)-3, ".k")


##info_filename = ["WaveformPattern_", num2str(distortion*100),"%.csv"];
##%info_filename = ["WaveformPattern_Cristalina1.csv"];
##%info_filename = ["WaveformPattern_Cristalina2.csv"];
##%info_filename = ["WaveformPattern_Cristalina3.csv"];
##output_string = ["#Digilent WaveForms Oscilloscope Acquisition\n",...
##                  "#Device Name: Discovery2\n",...
##                  "#Serial Number: SN:210321A29C1C\n",...
##                  "#Date Time: 2017-02-06 11:20::08.989\n",...
##                  "\n",...
##                  "Time (s),Channel 1 (V),Channel 2 (V)\n"];
##
##fid = fopen(info_filename,'w'); 
##fprintf(fid, output_string)
##fclose(fid);
##%write data to end of file
##dlmwrite(info_filename,[time_array_adj', hole_adj', seed_adj'],'-append');

clear time_array 
time_array = time_array_adj;
seed_voltage = seed_adj;
hole_voltage = hole_adj;

clear -x time_array seed_voltage hole_voltage

