clc
close all
clear -x optical_sensor_struct
more off
addpath("./functions")
addpath("./scripts")

%Load existing database
if (exist("DiscCharacteristics.mat") && !exist("disc_characteristics"))
  disp("Loading existing disc database...")
  load "DiscCharacteristics.mat"
  disp("Done");
endif

if (!exist("disc_characteristics"))
  char_sel = [];
  disc_characteristics = struct(...
    "sensor",         {},...
    "sens_idx",       {},...
    "disc",           {},...
    "disc_idx",       {},...
    "ring",           {},...
    "ring_idx",       {},...
    "neatness",       {},...
    "neat_idx",       {},...
    "local_extrema",  {}...
    );
endif


if (exist("GlobalVariables.mat") && !exist("vtit_idx"))
  disp("Loading global variables...")
  load "GlobalVariables.mat"
  disp("Done");
endif

if (exist("DatabaseStruct.mat") && !exist("optical_sensor_struct"))
  disp("Loading existing database...")
  load "DatabaseStruct.mat"
  disp("Done");
endif




%% Check if selection vector is set, if not select all
if (!exist("vtit_sel")) vtit_sel = 1:vtit_max; endif
vtit_sel = 1:vtit_max;
if (!exist("sens_sel")) sens_sel = 1:sens_max; endif
sens_sel = (1:2);
if (!exist("disc_sel")) disc_sel = 1:disc_max; endif
##disc_sel = 33; %sorgo
##disc_sel = [22, 24]; %soja
##disc_sel = 30; %feijao
disc_sel = 1:disc_max;
if (!exist("ring_sel")) ring_sel = 1:ring_max; endif
##ring_sel = 12; %sorgo
##ring_sel = 8; %soja
##ring_sel = 1; %feijao
ring_sel = 1:ring_max;
if (!exist("seed_sel")) seed_sel = 1:seed_max; endif
seed_sel = 1;
if (!exist("neat_sel")) neat_sel = 1:neat_max; endif
if (!exist("lens_sel")) lens_sel = 1:lens_max; endif

new_info = 0; 

filelist = {};
for char_idx = 1:length(disc_characteristics)
  for i = 1:size(disc_characteristics(char_idx).local_extrema.filename, 1)
    filelist(length(filelist)+1) = strtrim(disc_characteristics(char_idx).local_extrema.filename(i,:));
  endfor
endfor

char_idx = 0;

for vtit_idx = vtit_sel
##  disp(["Starting analysis of Visum Titanium ", num2str(vtit_idx), " out of ", num2str(length(vtit_sel))]);
  for sens_idx = sens_sel
    
    wave_max = length(optical_sensor_struct(vtit_idx, sens_idx).wave_database);
    for wave_idx = 1:wave_max
      
      %Find if wave is in selection
      if (!any(cell2mat(strfind(strtrim([visumtitanium_list{vtit_idx}, "/", opticalsensor_list{sens_idx},"/csv/",...
                                optical_sensor_struct(vtit_idx, sens_idx).wave_database(wave_idx).filename]), filelist))) && %file is not in disc database
          any(cell2mat(strfind(optical_sensor_struct(vtit_idx, sens_idx).wave_database(wave_idx).disc,             disc_list(disc_sel)))) &&... %disc in selection
          any(cell2mat(strfind(optical_sensor_struct(vtit_idx, sens_idx).wave_database(wave_idx).ring,             ring_list(ring_sel)))) &&... %ring in selection
          any(cell2mat(strfind(optical_sensor_struct(vtit_idx, sens_idx).wave_database(wave_idx).seedpresence,     seedpresence_list(seed_sel)))) &&... %seed in selection
          any(cell2mat(strfind(optical_sensor_struct(vtit_idx, sens_idx).wave_database(wave_idx).neatness,         neatness_list(neat_sel)))) &&... %neat in selection
          any(cell2mat(strfind(optical_sensor_struct(vtit_idx, sens_idx).wave_database(wave_idx).lenstransparency, lenstransparency_list(lens_sel))))) %lens in selection

          new_info = 1;        
          % Finding disc, ring and neat idx
          disc_idx = find(~cellfun(@isempty,strfind(optical_sensor_struct(vtit_idx,sens_idx).wave_database(wave_idx).disc, disc_list)));
          ring_idx = find(~cellfun(@isempty,strfind(optical_sensor_struct(vtit_idx,sens_idx).wave_database(wave_idx).ring, ring_list)));
          neat_idx = find(~cellfun(@isempty,strfind(optical_sensor_struct(vtit_idx,sens_idx).wave_database(wave_idx).neatness, neatness_list)));
                    
          char_found = 0;
          for i = 1:size(char_sel,1)
            if all(char_sel(i,:) == [sens_idx, disc_idx, ring_idx, neat_idx])
              char_idx = i;
              char_found = 1;
              disp(["  ", visumtitanium_list{vtit_idx}," Adding wave to ",...
                    opticalsensor_list{sens_idx}, " ", disc_list{disc_idx}, " ", ring_list{ring_idx}, " ", neatness_list{neat_idx}]);
              break;  
            endif
          endfor
          
          if (!char_found) 
            char_sel = [char_sel; sens_idx, disc_idx, ring_idx, neat_idx];
            char_idx = size(char_sel,1);
            disp(["New category: " opticalsensor_list{sens_idx}, " ", disc_list{disc_idx}, " ", ring_list{ring_idx}, " ", neatness_list{neat_idx}]);
              disp(["  ", visumtitanium_list{vtit_idx}," Adding wave to ",...
                    opticalsensor_list{sens_idx}, " ", disc_list{disc_idx}, " ", ring_list{ring_idx}, " ", neatness_list{neat_idx}]);
            local_extrema(char_idx) = struct(...
              "max_max",   [],...
              "avg_max",   [],...
              "min_max",   [],...
              "max_min",   [],...
              "avg_min",   [],...
              "min_min",   [],...
              "whiteAmps", [],...
              "filename",  [""]...
              );
          endif
          
##        if strcmp(strtrim(optical_sensor_struct(vtit_idx,sens_idx).wave_database(wave_idx).neatness), "clean")
##          neat_idx = 1;          
##        else
##          neat_idx = 2;
##        endif
        


        
        for svec_idx = 1:length(optical_sensor_struct(vtit_idx, sens_idx).wave_database(wave_idx).seed_vector)
          time_array = optical_sensor_struct(vtit_idx, sens_idx).wave_database(wave_idx).time_array;
          start_idx = lookup(time_array, optical_sensor_struct(vtit_idx, sens_idx).wave_database(wave_idx).wave_window_time(svec_idx));
          end_idx = lookup(time_array, optical_sensor_struct(vtit_idx, sens_idx).wave_database(wave_idx).wave_window_time(svec_idx+1));
          

          avg_size = 10;
          m = 1;
          if (sens_idx == 1)% && strcmp(strtrim(optical_sensor_struct(vtit_idx, sens_idx).seed_phototransPN), "BPW85A"))
            for k = start_idx:end_idx
              seed_smooth(m) = mean(optical_sensor_struct(vtit_idx, sens_idx).wave_database(wave_idx).seed_amps(max(k-avg_size, 1):k));
              m += 1;
            endfor % moving average on current data
##            plot(optical_sensor_struct(vtit_idx, sens_idx).wave_database(wave_idx).seed_amps(start_idx:end_idx));
##            hold on
##            plot(seed_smooth, "r")
##            waitfor(gcf);

            max_wave(svec_idx) = max(seed_smooth);
            min_wave(svec_idx) = min(seed_smooth);
            white_current = (optical_sensor_struct(vtit_idx, sens_idx).seed_whiteAmps);
          elseif (sens_idx == 2)% && strcmp(strtrim(optical_sensor_struct(vtit_idx, sens_idx).hole_phototransPN), "BPW85A"))
            for k = start_idx:end_idx
              hole_smooth(m) = mean(optical_sensor_struct(vtit_idx, sens_idx).wave_database(wave_idx).hole_amps(max(k-avg_size, 1):k));
              m += 1;
            endfor % moving average on current data
            
##            plot(optical_sensor_struct(vtit_idx, sens_idx).wave_database(wave_idx).hole_amps(start_idx:end_idx));
##            hold on
##            plot(hole_smooth, "r")
##            waitfor(gcf);
            
            max_wave(svec_idx) = max(hole_smooth);
            min_wave(svec_idx) = min(hole_smooth);
            white_current = (optical_sensor_struct(vtit_idx, sens_idx).hole_whiteAmps);
          endif %sens_idx
          clear seed_smooth hole_smooth
##          if (max_wave != 0)
##            if (neat_idx == 1)
##              max_wave_clean(i) = max_wave;
##              min_wave_clean(i) = min_wave;
####              [max_wave_clean(i), ~, ~] = hardware_model(struct("seed_amps", max_wave, "seed_resistor", target_resistor, "time_array", 1,...
####                                                                "hole_amps", 0,        "hole_resistor", 0,      "supply_voltage", 3));
####              max_wave_clean(i) = max_wave_clean(i) * 1024 / 3;
####              [min_wave_clean(i), ~, ~] = hardware_model(struct("seed_amps", min_wave, "seed_resistor", target_resistor, "time_array", 1,...
####                                                                "hole_amps", 0,        "hole_resistor", 0,      "supply_voltage", 3));
####              min_wave_clean(i) = min_wave_clean(i) * 1024 / 3;
##              white_current_clean(i) = white_current;
##              i = i+1;
##            else
##              max_wave_dirty(j) = max_wave;
##              min_wave_dirty(j) = min_wave;
####              [~, max_wave_dirty(j), ~] = hardware_model(struct("seed_amps", 0,               "seed_resistor", 0, "time_array", 1,...
####                                                                "hole_amps", max_wave,        "hole_resistor", target_resistor,      "supply_voltage", 3));
####              max_wave_dirty(j) = max_wave_dirty(j) * 1024 / 3;
####              [~, min_wave_dirty(j), ~] = hardware_model(struct("seed_amps", 0,               "seed_resistor", 0, "time_array", 1,...
####                                                                "hole_amps", min_wave,        "hole_resistor", target_resistor,      "supply_voltage", 3));
####              min_wave_dirty(j) = min_wave_dirty(j) * 1024 / 3;
##              white_current_dirty(j) = white_current;
##              j = j+1;
##            endif
##          endif
        endfor %svec_idx
        [~, min_idx] = min(min_wave); 
        min_wave(min_idx) = [];
        
        local_extrema(char_idx) = struct(...
          "max_max",   [local_extrema(char_idx).max_max,  max(max_wave)],...
          "avg_max",   [local_extrema(char_idx).avg_max,  mean(max_wave)],...
          "min_max",   [local_extrema(char_idx).min_max,  min(max_wave)],...
          "max_min",   [local_extrema(char_idx).max_min,  max(min_wave)],...
          "avg_min",   [local_extrema(char_idx).avg_min,  mean(min_wave)],...
          "min_min",   [local_extrema(char_idx).min_min,  min(min_wave)],...
          "whiteAmps", [local_extrema(char_idx).whiteAmps,white_current],...
          "filename",  [local_extrema(char_idx).filename;...
                        [visumtitanium_list{vtit_idx}, "/", opticalsensor_list{sens_idx},"/csv/",...
                        optical_sensor_struct(vtit_idx, sens_idx).wave_database(wave_idx).filename]]...
          );
         
          
        
        disc_characteristics(char_idx) = struct(...
          "sensor",        opticalsensor_list{sens_idx},...
          "sens_idx",      sens_idx,...
          "disc",          optical_sensor_struct(vtit_idx, sens_idx).wave_database(wave_idx).disc,...
          "disc_idx",      disc_idx,...
          "ring",          optical_sensor_struct(vtit_idx, sens_idx).wave_database(wave_idx).ring,...
          "ring_idx",      ring_idx,...
          "neatness",      optical_sensor_struct(vtit_idx, sens_idx).wave_database(wave_idx).neatness,...
          "neat_idx",      neat_idx,...
          "local_extrema", local_extrema(char_idx)...
          );
          
        clear max_wave min_wave white_current
      endif 
    endfor
  endfor
endfor

## Save if there is new info
if (new_info)
  disp("Saving new files on disc database...")
  save "DiscCharacteristics.mat" disc_characteristics local_extrema char_sel
endif
disp("Done")

supply_voltage = 3;
##target_resistor = [166000, 297000, 470000];
target_resistor = [297000];
limit_V = [70, 70; 800, 900] * 3 / 1024;

for i = 1:length(target_resistor)
##  linear_coefficient = 0.1969 * target_resistor(i) + 88853;
  coefficients = [166000  208000  255000  297000  374000  470000  806000;
                  109594  122159  145028  156605  172608  180965  242266];
  linear_coefficient = coefficients(2,find(target_resistor(i) == coefficients(1,:)));
 
  saturation_coefficient = 563;
  saturation_constant = 0.2288;
  
  intersect_A = (supply_voltage - saturation_constant) / (linear_coefficient - saturation_coefficient);
  intersect_V = supply_voltage - linear_coefficient * intersect_A;
  
  for j = 1:size(limit_V,1)
    for k = 1:size(limit_V, 2)
      if (limit_V(j,k) >= intersect_V)
        limit_A(j,k, i) = (supply_voltage - limit_V(j,k)) / linear_coefficient;
      else
        limit_A(j,k, i) = (saturation_constant - limit_V(j,k)) / saturation_coefficient;
      endif
    endfor
  endfor
endfor





figure
for char_idx = 1:length(disc_characteristics)

    
    if(disc_characteristics(char_idx).neat_idx == 1 && disc_characteristics(char_idx).sens_idx == 1) %clean + sem
      plot_style = ".c";
    elseif(disc_characteristics(char_idx).neat_idx == 2 && disc_characteristics(char_idx).sens_idx == 1) %dirty + sem
      plot_style = ".b";
    elseif(disc_characteristics(char_idx).neat_idx == 1 && disc_characteristics(char_idx).sens_idx == 2) %clean + furo
      plot_style = ".m";
    elseif(disc_characteristics(char_idx).neat_idx == 2 && disc_characteristics(char_idx).sens_idx == 2) %dirty + furo
      plot_style = ".r";
    endif
    
    if (disc_characteristics(char_idx).disc_idx == 33) %sorgo
      plot_idx = 1;
      handle_idx = 1;
    elseif any(disc_characteristics(char_idx).disc_idx == [22,24]) %soja     
      plot_idx = 3;
      handle_idx = 2;
    elseif (disc_characteristics(char_idx).disc_idx == 30) % feijao
      plot_idx = 5;
      handle_idx = 3;
    endif
    
    subplot(3,2,plot_idx)
    plot(disc_characteristics(char_idx).local_extrema.whiteAmps, disc_characteristics(char_idx).local_extrema.avg_max, ".b");
    errorbar(disc_characteristics(char_idx).local_extrema.whiteAmps,...
             10^6*disc_characteristics(char_idx).local_extrema.avg_max,...
             10^6*(-disc_characteristics(char_idx).local_extrema.min_max + disc_characteristics(char_idx).local_extrema.avg_max),...
             10^6*(disc_characteristics(char_idx).local_extrema.max_max -  disc_characteristics(char_idx).local_extrema.avg_max),...
             plot_style);
    hold on
    subplot(3,2,plot_idx+1)
    plot(disc_characteristics(char_idx).local_extrema.whiteAmps, disc_characteristics(char_idx).local_extrema.avg_min, ".b");
    errorbar(disc_characteristics(char_idx).local_extrema.whiteAmps,...
             10^6*disc_characteristics(char_idx).local_extrema.avg_min,...
             10^6*(-disc_characteristics(char_idx).local_extrema.min_min + disc_characteristics(char_idx).local_extrema.avg_min),...
             10^6*(disc_characteristics(char_idx).local_extrema.max_min  - disc_characteristics(char_idx).local_extrema.avg_min),...
             plot_style);
    hold on
endfor

%% PLOT MAXIMUMS
subplot(3,2,1)
title("Sorgo - max")
xlabel("White current (uA)")
ylabel("Current on disc (uA)")
grid on
axis([0, 700, 0, 250])

plot([0, 700, 700, 0], [limit_A(1,1,1) limit_A(1,1,1) limit_A(1,2,1) limit_A(1,2,1)] *10^6);
text(700, 10^6*limit_A(1,1,1), [num2str(limit_V(1,1) *1024/3) "   "])
text(700, 10^6*limit_A(1,2,1), [num2str(limit_V(1,2) *1024/3) "   "])
##for i = 1:size(limit_A, 3)
##  plot([0, 700, 700, 0], [limit_A(2,1,i) limit_A(2,1,i) limit_A(2,2,i) limit_A(2,2,i)] *10^6);
##  text(600, 10^6*limit_A(2,1,i)+0.3, [num2str(limit_V(2,1) *1024/3)])
##  text(600, 10^6*limit_A(2,2,i)+0.3, [num2str(limit_V(2,2) *1024/3) "   ", num2str(target_resistor(i)), " Ohm"])
##endfor


subplot(3,2,3)
title("Soja - max")
xlabel("White current (uA)")
ylabel("Current on disc (uA)")
grid on
axis([0, 700, 0, 250])

plot([0, 700, 700, 0], [limit_A(1,1,1) limit_A(1,1,1) limit_A(1,2,1) limit_A(1,2,1)] *10^6);
text(700, 10^6*limit_A(1,1,1), [num2str(limit_V(1,1) *1024/3) "   "])
text(700, 10^6*limit_A(1,2,1), [num2str(limit_V(1,2) *1024/3) "   "])
##for i = 1:size(limit_A, 3)
##  plot([0, 700, 700, 0], [limit_A(2,1,i) limit_A(2,1,i) limit_A(2,2,i) limit_A(2,2,i)] *10^6);
##  text(600, 10^6*limit_A(2,1,i)+0.3, [num2str(limit_V(2,1) *1024/3)])
##  text(600, 10^6*limit_A(2,2,i)+0.3, [num2str(limit_V(2,2) *1024/3) "     ", num2str(target_resistor(i)), " Ohm"])
##endfor

subplot(3,2,5)
title("Feijao - max")
xlabel("White current (uA)")
ylabel("Current on disc (uA)")
grid on
axis([0, 700, 0, 250])

plot([0, 700, 700, 0], [limit_A(1,1,1) limit_A(1,1,1) limit_A(1,2,1) limit_A(1,2,1)] *10^6);
text(700, 10^6*limit_A(1,1,1), [num2str(limit_V(1,1) *1024/3) "   "])
text(700, 10^6*limit_A(1,2,1), [num2str(limit_V(1,2) *1024/3) "   "])
##for i = 1:size(limit_A, 3)
##  plot([0, 700, 700, 0], [limit_A(2,1,i) limit_A(2,1,i) limit_A(2,2,i) limit_A(2,2,i)] *10^6);
##  text(600, 10^6*limit_A(2,1,i)+0.3, [num2str(limit_V(2,1) *1024/3)])
##  text(600, 10^6*limit_A(2,2,i)+0.3, [num2str(limit_V(2,2) *1024/3)])
##endfor




%% PLOT MINIMUMS
subplot(3,2,2)
title("Sorgo - min")
xlabel("White current (uA)")
ylabel("Current on disc (uA)")
grid on
axis([0, 700, 0, 20])

plot([0, 700, 700, 0], [limit_A(1,1,1) limit_A(1,1,1) limit_A(1,2,1) limit_A(1,2,1)] *10^6);
##text(700, 10^6*limit_A(1,1,1), [num2str(limit_V(1,1) *1024/3) "   "])
##text(700, 10^6*limit_A(1,2,1), [num2str(limit_V(1,2) *1024/3) "   "])
for i = 1:size(limit_A, 3)
  plot([0, 700, 700, 0], [limit_A(2,1,i) limit_A(2,1,i) limit_A(2,2,i) limit_A(2,2,i)] *10^6);
  text(600, 10^6*limit_A(2,1,i)+0.3, [num2str(limit_V(2,1) *1024/3)])
  text(600, 10^6*limit_A(2,2,i)+0.3, [num2str(limit_V(2,2) *1024/3) "   ", num2str(target_resistor(i)), " Ohm"])
endfor


subplot(3,2,4)
title("Soja - min")
xlabel("White current (uA)")
ylabel("Current on disc (uA)")
grid on
axis([0, 700, 0, 20])

plot([0, 700, 700, 0], [limit_A(1,1,1) limit_A(1,1,1) limit_A(1,2,1) limit_A(1,2,1)] *10^6);
##text(700, 10^6*limit_A(1,1,1), [num2str(limit_V(1,1) *1024/3) "   "])
##text(700, 10^6*limit_A(1,2,1), [num2str(limit_V(1,2) *1024/3) "   "])
for i = 1:size(limit_A, 3)
  plot([0, 700, 700, 0], [limit_A(2,1,i) limit_A(2,1,i) limit_A(2,2,i) limit_A(2,2,i)] *10^6);
  text(600, 10^6*limit_A(2,1,i)+0.3, [num2str(limit_V(2,1) *1024/3)])
  text(600, 10^6*limit_A(2,2,i)+0.3, [num2str(limit_V(2,2) *1024/3) "     ", num2str(target_resistor(i)), " Ohm"])
endfor

subplot(3,2,6)
title("Feijao - min")
xlabel("White current (uA)")
ylabel("Current on disc (uA)")
grid on
axis([0, 700, 0, 20])

plot([0, 700, 700, 0], [limit_A(1,1,1) limit_A(1,1,1) limit_A(1,2,1) limit_A(1,2,1)] *10^6);
##text(700, 10^6*limit_A(1,1,1), [num2str(limit_V(1,1) *1024/3) "   "])
##text(700, 10^6*limit_A(1,2,1), [num2str(limit_V(1,2) *1024/3) "   "])
for i = 1:size(limit_A, 3)
  plot([0, 700, 700, 0], [limit_A(2,1,i) limit_A(2,1,i) limit_A(2,2,i) limit_A(2,2,i)] *10^6);
  text(600, 10^6*limit_A(2,1,i)+0.3, [num2str(limit_V(2,1) *1024/3)])
  text(600, 10^6*limit_A(2,2,i)+0.3, [num2str(limit_V(2,2) *1024/3)])
endfor

